package com.github.xuejike.query.core.enums;



/**
 * @author xuejike
 * @date 2020/12/31
 */
public enum LoadRefMode {
    noCache,useCache
}
