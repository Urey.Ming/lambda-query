package com.github.xuejike.query.core.criteria;

import com.github.xuejike.query.core.enums.LoadRefMode;
import com.github.xuejike.query.core.tool.lambda.FieldFunction;

/**
 * @author xuejike
 * @date 2020/12/31
 */
public interface LoadRefCriteria<F,R>{

    /**
     * 加载引用
     * @param refField 引用字段
     * @param entityCls 目标实体类
     * @param targetField 目标被引用字段
     * @param mode 是否使用缓存
     * @param selectedFields 筛选字段
     * @param <X>
     * @return
     */
    <X> R loadRef(F refField, Class<X> entityCls, FieldFunction<X,?> targetField, LoadRefMode mode,FieldFunction<X,?> ... selectedFields);
    default<X> R loadRef(F refField, Class<X> entityCls, FieldFunction<X,?> targetField){
        return loadRef(refField, entityCls,targetField,LoadRefMode.noCache);
    }
    default<X> R loadRef(F refField, Class<X> entityCls, FieldFunction<X,?> targetField,FieldFunction<X,?> ... selectedFields){
        return loadRef(refField, entityCls,targetField,LoadRefMode.noCache,selectedFields);
    }
}
